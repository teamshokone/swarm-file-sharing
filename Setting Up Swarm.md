If we make an app utilizing Swarm, the following actions are necessary:
(https://swarm-guide.readthedocs.io/en/latest/installation.html)

Installation Requirements:
-Install golang and git
-Set paths of Go (GOPATH and GOROOT)

Installation of Swarm:
-Make directory for go-ethereum source code
-Use git to clone go-ethereum
-Make go-ethereum checkout master
-Use “go get” command to download go-ethereum to new directory
-use “go install” to install geth and swarm (go install -v ./cmd/geth) (go install -v ./cmd/swarm)

Launching of Swarm:
-Create a data directory
(mkdir Desktop/Swarm/dataDirectory)
-Set up a geth account with datadir option
(~/go/bin/geth --datadir /home/user/Desktop/Swarm/dataDirectory/ account new)
-Prompt password creation
-Launch a geth node to run in the background
(~/go/bin/geth --fast --datadir ~/home/user/Desktop/Swarm/dataDirectory/)
-Start Swarm with 3 options:
	-bzzaccount
	-path to datadir
	-path to geth.ipc
(~/go/bin/swarm --bzzaccount [account address] --datadir ~/home/user/Desktop/Swarm/dataDirectory/ --ethapi ~/home/user/Desktop/Swarm/dataDirectory/geth.ipc)
